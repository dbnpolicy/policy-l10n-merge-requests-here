Source: debian-policy
Maintainer: Debian Policy Editors <debian-policy@lists.debian.org>
Uploaders:
 Andreas Barth <aba@debian.org>,
 Bill Allombert <ballombe@debian.org>,
 Sean Whitton <spwhitton@spwhitton.name>,
 Russ Allbery <rra@debian.org>,
Section: doc
Priority: optional
Build-Depends:
 dblatex,
 debhelper (>= 10),
 dh-linktree,
 dia,
 docbook-xml,
 docbook-xsl,
 latexmk,
 libtext-multimarkdown-perl,
 libxml2-utils,
 links | elinks,
 python3-sphinx,
 sphinx-common (>= 1.6.5),
 sphinx-intl,
 texinfo,
 xsltproc,
Standards-Version: 4.2.0
Vcs-Browser: https://salsa.debian.org/dbnpolicy/policy
Vcs-Git: https://salsa.debian.org/dbnpolicy/policy

Package: debian-policy
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Suggests:
 doc-base,
Description: Debian Policy Manual and related documents
 This package contains:
    - Debian Policy Manual
    - Filesystem Hierarchy Standard (FHS)
    - Debian Menu sub-policy
    - Debian Perl sub-policy
    - Debian configuration management specification
    - Machine-readable debian/copyright specification
    - Autopkgtest - automatic as-installed package testing
    - Authoritative list of virtual package names
    - Policy checklist for upgrading your packages
 It also replaces the old Packaging Manual; most of the still-relevant
 content is now included as appendices to the Policy Manual.

Package: debian-policy-ja
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Suggests:
 doc-base,
Description: Debian Policy Manual and related documents (Japanese)
 This package contains translations into Japanese of some of the
 documents distributed in the debian-policy package.  Currently only
 the HTML output format is available.
